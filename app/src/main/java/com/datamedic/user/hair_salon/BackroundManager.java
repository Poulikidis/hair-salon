package com.datamedic.user.hair_salon;

/**
 * Created by userx on 22/8/2017.
 */

public class BackroundManager {


    private static BackroundManager dataManager;
    private String imageUrl;

    public static BackroundManager getInstance(){

        if(dataManager == null){
            dataManager = new BackroundManager();
        }
        return dataManager;
    }

    private BackroundManager(){}

    public String getImageUrl(){
        return imageUrl;
    }

    public void setImageUrl(String imageUrl){
        this.imageUrl = imageUrl;
    }



}
