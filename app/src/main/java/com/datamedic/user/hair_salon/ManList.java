package com.datamedic.user.hair_salon;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.datamedic.user.hair_salon.R;

import java.io.IOException;
import java.util.ArrayList;

public class ManList extends MainActivity {
    Database database;
    RotateResizeImage rotateResizeImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);
        database = new Database(this);
        final Cursor res = database.getMan();

        final ListView list = findViewById(R.id.list);

        rotateResizeImage = new RotateResizeImage();

        sharedPreferences = this.getSharedPreferences("com.datamedic.userx.ratingapp", Context.MODE_PRIVATE);
        String picturePath = sharedPreferences.getString("picturepath1","");
        ImageView imageView = findViewById(R.id.imageView);

        if(picturePath == null || picturePath.trim().equals("")){

        }else {
            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            rotateResizeImage.getResizedBitmap(bitmap,150,150);
            BitmapDrawable background = null;
            try {
                background = new BitmapDrawable(rotateResizeImage.modifyOrientation(bitmap, picturePath));

            } catch (IOException e) {
                e.printStackTrace();
            }
            imageView.setBackground(background);
        }
//        BlurImage.with(getApplicationContext()).load(R.drawable.marinos).intensity(20).Async(true).into(imageView);




        final ArrayList<ManClass> theList = new ArrayList<>();


        if (res.getCount() == 0)
            Toast.makeText(ManList.this, R.string.norecordsindb, Toast.LENGTH_SHORT).show();
        else
            while (res.moveToNext()) {

                ManClass man = new ManClass(res.getString(1) , res.getString(2) , res.getString(3),res.getString(0) );

//                theList.add(res.getString(1) + " " + res.getString(2) + " Height : " + res.getString(3) + "  KG :" + res.getString(4));
                theList.add(man);
                ManClassList manClassList = new ManClassList(this, R.layout.listview,theList);
                list.setAdapter(manClassList);


            }
            list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, final long id) {

                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(ManList.this);
                    View mView = getLayoutInflater().inflate(R.layout.delete, null);

                    final String idman = theList.get(position).getId();
                    Button Yes = mView.findViewById(R.id.YesDelete);
                    Button No = mView.findViewById(R.id.NoDelete);
                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();

                    dialog.show();
                    Yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            database.delete(idman);
                            dialog.cancel();
                           idman.equals("");
                            Intent i = new Intent(ManList.this, Man_Women.class);
                            startActivity(i);
                            finish();
                        }
                    });
                    No.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();


                        }
                    });





                    return false;
                }
            });




    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), Man_Women.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }


//                while (res.moveToNext()){
//                    buffer.append("ΗΜΕΡΟΜΗΝΙΑ:"+res.getString(1)+"\n");
//                    buffer.append("ΥΠΗΡΕΣΙΕΣ:"+res.getString(2)+"\n");
//                    buffer.append("ΜΕΤΑΠΩΛΗΣΗ:"+res.getString(3)+"\n");
//                }

}
