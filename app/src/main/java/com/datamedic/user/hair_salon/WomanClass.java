package com.datamedic.user.hair_salon;

public class WomanClass {
    String imeromonia;
    String kourema;
    String xtenisma;
    String bafi;
    String metapolisi;

    private String Id;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getXtenisma() {
        return xtenisma;
    }

    public void setXtenisma(String xtenisma) {
        this.xtenisma = xtenisma;
    }

    public String getBafi() {
        return bafi;
    }

    public void setBafi(String bafi) {
        this.bafi = bafi;
    }

    public String getImeromonia() {
        return imeromonia;
    }

    public void setImeromonia(String imeromonia) {
        this.imeromonia = imeromonia;
    }

    public String getKourema() {
        return kourema;
    }

    public void setKourema(String kourema) {
        this.kourema = kourema;
    }

    public String getMetapolisi() {
        return metapolisi;
    }

    public void setMetapolisi(String metapolisi) {
        this.metapolisi = metapolisi;
    }


    public WomanClass(String imeromonia, String kourema,String xtenisma,String bafi, String metapolisi, String id){
       this.imeromonia = imeromonia;
       this.kourema = kourema;
       this.xtenisma = xtenisma;
       this.bafi = bafi;
       this.metapolisi = metapolisi;
       this.Id = id;
    }

}
