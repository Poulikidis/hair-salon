package com.datamedic.user.hair_salon;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;


public class Database extends SQLiteOpenHelper {


    public static final String DATABASE_NAME = "hair_salon_wallet_db.dp";
    public static final String MAN_TABLE = "MAN_TABLE";
    public static final String IDMAN = "IDMAN";
    public static final String CALENDARMAN = "ΗΜΕΡΟΜΗΝΙΑ_ΑΝ";
    public static final String YPIRESIES = "ΥΠΗΡΕΣΙΕΣ";
    public static final String METAPOLISIS = "ΜΕΤΑΠΩΛΗΣΗ_ΑΝ";
    public static final String DATE = "DATE";

    public static final String WOMAN_TABLE = "WOMAN_TABLE";
    public static final String IDWOMAN = "IDWOMAN";
    public static final String CALENDARWOMAN = "ΗΜΕΡΟΜΗΝΙΑ_ΓΥΝ";
    public static final String KOUREMA = "ΚΟΥΡΕΜΑ";
    public static final String XTENISMA = "ΧΤΕΝΙΣΜΑ";
    public static final String BAFI = "ΒΑΦΗ";
    public static final String METAPOLISIS2 = "ΜΕΤΑΠΩΛΗΣΗΣ_ΓΥΝ";
    public static final String DATE2 = "DATE2";

    public static final String EXPENSES_TABLE = "EXPENSES_TABLE";
    public static final String IDEXPENSES = "IDEXPENSES";
    public static final String CALENDAREXPEMSES = "CALENDAREXPEMSES";
    public static final String EXPENSES_WHAT = "EXPENSES_WHAT";
    public static final String EXPENSES = "EXPENSES";
    public static final String DATE3 = "DATE3";


    public Database(Context context) {
        super(context, DATABASE_NAME, null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        Log.d("DATABASE", "DEN EIDE TIN ALLAGI BASIS");

        db.execSQL("create table " + MAN_TABLE + " (" + IDMAN + " INTEGER PRIMARY KEY AUTOINCREMENT," + CALENDARMAN + " TEXT," + YPIRESIES +
                " DOUBLE," + METAPOLISIS + " DOUBLE,DATE LONG)");
        db.execSQL("create table " + WOMAN_TABLE + " (" + IDWOMAN + " INTEGER PRIMARY KEY AUTOINCREMENT," + CALENDARWOMAN + " TEXT,"
                + KOUREMA + " DOUBLE," + XTENISMA + " DOUBLE," + BAFI + " DOUBLE," + METAPOLISIS2 + " DOUBLE,DATE2 LONG)");
        db.execSQL("create table EXPENSES_TABLE  (IDEXPENSES INTEGER PRIMARY KEY AUTOINCREMENT,CALENDAREXPEMSES TEXT,EXPENSES_WHAT TEXT,EXPENSES DOUBLE,DATE3 LONG)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        switch(oldVersion) {
            case 1:
                if (oldVersion == 1 && newVersion == 2){

                    Log.d("DATABASE", "EIDE THN ALLAGI EKDOSIS");

                    db.execSQL("create table EXPENSES_TABLE  (IDEXPENSES INTEGER PRIMARY KEY AUTOINCREMENT,CALENDAREXPEMSES TEXT,EXPENSES_WHAT TEXT,EXPENSES DOUBLE,DATE3 LONG)");

                }
            case 2:

                break;
            case 3:

                break;
            default:
                throw new IllegalStateException(
                        "onUpgrade() with unknown oldVersion " + oldVersion);
        }


    }

    public Boolean data(String cal, double ypiresies, double metapolisis, long date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CALENDARMAN, cal);
        contentValues.put(YPIRESIES, ypiresies);
        contentValues.put(METAPOLISIS, metapolisis);
        contentValues.put(DATE, date);

        long result = db.insert(MAN_TABLE, null, contentValues);
        if (result == -1)
            return false;
        else return true;

    }

    public Boolean data2(String cal, double kourema, double xtenisma, double bafi, double metapolisi, long date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CALENDARWOMAN, cal);
        contentValues.put(KOUREMA, kourema);
        contentValues.put(XTENISMA, xtenisma);
        contentValues.put(BAFI, bafi);
        contentValues.put(METAPOLISIS2, metapolisi);
        contentValues.put(DATE2, date);

        long result = db.insert(WOMAN_TABLE, null, contentValues);
        if (result == -1)
            return false;
        else return true;

    }

    public Boolean data3(String cal,String what, double expenses, long date) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CALENDAREXPEMSES, cal);
        contentValues.put(EXPENSES_WHAT,what);
        contentValues.put(EXPENSES, expenses);
        contentValues.put(DATE3, date);

        long result = db.insert(EXPENSES_TABLE, null, contentValues);
        if (result == -1)
            return false;
        else return true;
    }


    public double selectExpenses(String column,double from, double to){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select sum(" + column + ") from " + EXPENSES_TABLE + " where "
                + DATE3 + " between " + from + " and " + to;

        double sum = 0;
        Cursor res = db.rawQuery(query, null);
        res.moveToFirst();
        if (res.getCount() > 0) {
            sum = res.getDouble(0);
        }
        return sum;
    }


    public double selectMan(String column, double from, double to) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select sum(" + column + ") from " + MAN_TABLE + " where "
                + DATE + " between " + from + " and " + to;

        double sum = 0;
        Cursor res = db.rawQuery(query, null);
        res.moveToFirst();
        if (res.getCount() > 0) {
            sum = res.getDouble(0);
        }
        return sum;
    }

    public double selectWomen(String column, long from, long to) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select sum(" + column + ") from " + WOMAN_TABLE + " where "
                + DATE2 + " between " + from + " and " + to;

        double sum = 0;
        Cursor res = db.rawQuery(query, null);
        res.moveToFirst();
        if (res.getCount() > 0) {
            sum = res.getDouble(0);
        }
        return sum;

    }

    public double selectExpensesAll(long from, long to) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select sum( "+ EXPENSES + ") from " + EXPENSES_TABLE + "  where "
                + DATE3 + " between " + from + " and " + to, null);

        double total = 0;
        if (res.moveToFirst())
            total = res.getDouble(0);
        while (res.moveToNext())
            return total;
        return total;

    }

    public double selectManAll(long from, long to) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select sum(" + YPIRESIES + "+" + METAPOLISIS + ") from " + MAN_TABLE + "  where "
                + DATE + " between " + from + " and " + to, null);

        double total = 0;
        if (res.moveToFirst())
            total = res.getDouble(0);
        while (res.moveToNext())
            return total;
        return total;

    }

    public double selectWomanAll(long from, long to) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select sum(" + KOUREMA + "+" + XTENISMA + "+" + BAFI + "+" + METAPOLISIS2 + ") from " + WOMAN_TABLE + "  where "
                + DATE2 + " between " + from + " and " + to, null);

        double total = 0;
        if (res.moveToFirst())
            total = res.getDouble(0);
        while (res.moveToNext())
            return total;
        return total;

    }
    public Cursor getExpenses() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select * from " + "( select * from " + EXPENSES_TABLE + " order by "
                + IDEXPENSES + " desc limit " + 10 + " )" + " order by " + IDEXPENSES + " ASC ";

        Cursor res = db.rawQuery(query, null);
        return res;
    }

    public Cursor getMan() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select * from " + "( select * from " + MAN_TABLE + " order by "
                + IDMAN + " desc limit " + 10 + " )" + " order by " + IDMAN + " ASC ";

        Cursor res = db.rawQuery(query, null);
        return res;
    }

    public Cursor getWoman() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select * from " + "( select * from " + WOMAN_TABLE + " order by "
                + IDWOMAN + " desc limit " + 10 + " )" + " order by " + IDWOMAN + " ASC ";
        Cursor res = db.rawQuery(query, null);
        return res;
    }

    public ArrayList<HashMap<String, String>> GetUserByUserId(int userid) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap<String, String>> userList = new ArrayList<>();
        String query = "SELECT ΗΜΕΡΟΜΗΝΙΑ_ΑΝ, ΥΠΗΡΕΣΙΕΣ, ΜΕΤΑΠΩΛΗΣΗ_ΑΝ FROM " + MAN_TABLE;
        Cursor cursor = db.query(MAN_TABLE, new String[]{CALENDARMAN, YPIRESIES, METAPOLISIS}, IDMAN + "=?", new String[]{String.valueOf(userid)}, null, null, null, null);
        if (cursor.moveToNext()) {
            HashMap<String, String> user = new HashMap<>();
            user.put("ΗΜΕΡΟΜΗΝΙΑ_ΑΝ", cursor.getString(cursor.getColumnIndex(CALENDARMAN)));
            user.put("ΥΠΗΡΕΣΙΕΣ", cursor.getString(cursor.getColumnIndex(YPIRESIES)));
            user.put("ΜΕΤΑΠΩΛΗΣΗ_ΑΝ", cursor.getString(cursor.getColumnIndex(METAPOLISIS)));
            userList.add(user);
        }
        return userList;
    }

    public ArrayList<HashMap<String, String>> GetUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap<String, String>> userList = new ArrayList<>();
        String query = "SELECT ΗΜΕΡΟΜΗΝΙΑ_ΑΝ, ΥΠΗΡΕΣΙΕΣ, ΜΕΤΑΠΩΛΗΣΗ_ΑΝ FROM " + MAN_TABLE;
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            HashMap<String, String> user = new HashMap<>();
            user.put("ΗΜΕΡΟΜΗΝΙΑ_ΑΝ", cursor.getString(cursor.getColumnIndex(CALENDARMAN)));
            user.put("ΥΠΗΡΕΣΙΕΣ", cursor.getString(cursor.getColumnIndex(YPIRESIES)));
            user.put("ΜΕΤΑΠΩΛΗΣΗ_ΑΝ", cursor.getString(cursor.getColumnIndex(METAPOLISIS)));
            userList.add(user);
        }
        return userList;
    }

    public Integer delete(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(MAN_TABLE, "IDMAN = ?", new String[]{id});

    }

    public Integer delete2(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(WOMAN_TABLE, "IDWOMAN = ?", new String[]{id});

    }
    public Integer delete3(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(EXPENSES_TABLE, "IDEXPENSES = ?", new String[]{id});

    }

    public String getDatabaseName() {
        return DATABASE_NAME;
    }
}

