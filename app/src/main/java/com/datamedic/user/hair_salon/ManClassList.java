package com.datamedic.user.hair_salon;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ManClassList extends ArrayAdapter<ManClass> {
    private Context mcontext;
    private int mresource;

    Database database;

    public ManClassList(@NonNull Context context, int resource ,ArrayList object) {
        super(context, resource,object);
        mcontext = context;
        mresource = resource;
        database = new Database(context);



    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final String Imerominia = getItem(position).getImeromonia();
        final String Ipiresia = getItem(position).getIpiresies();
        final String Metapolis = getItem(position).getMetapolisi();
        final String ID = getItem(position).getId();


        LayoutInflater inflater = LayoutInflater.from(mcontext);
        convertView = inflater.inflate(mresource, parent, false);

        TextView ImerominaText = convertView.findViewById(R.id.calendarExpenses);
        TextView IpiresisaText = convertView.findViewById(R.id.expenses);
        TextView MetapolisiText = convertView.findViewById(R.id.xtenisma);




        ImerominaText.setText(Imerominia);
        IpiresisaText.setText(Ipiresia);
        MetapolisiText.setText(Metapolis);
        return convertView;
    }
    }

