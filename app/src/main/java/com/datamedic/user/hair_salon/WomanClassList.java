package com.datamedic.user.hair_salon;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class WomanClassList extends ArrayAdapter<WomanClass> {
    private Context mcontext;
    private int mresource;

    Database database;

    public WomanClassList(@NonNull Context context, int resource , ArrayList object) {
        super(context, resource,object);
        mcontext = context;
        mresource = resource;
        database = new Database(context);



    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final String Imerominia = getItem(position).getImeromonia();
        final String Kourema = getItem(position).getKourema();
        final String Xtenisma = getItem(position).getXtenisma();
        final String Bafi = getItem(position).getBafi();
        final String Metapolis = getItem(position).getMetapolisi();
        final String ID = getItem(position).getId();


        LayoutInflater inflater = LayoutInflater.from(mcontext);
        convertView = inflater.inflate(mresource, parent, false);

        TextView ImerominaText = convertView.findViewById(R.id.calendarExpenses);
        TextView KouremaText = convertView.findViewById(R.id.expenses);
        TextView XtenismaText = convertView.findViewById(R.id.xtenisma);
        TextView BafiText = convertView.findViewById(R.id.bafi);
        TextView MetapolisiText = convertView.findViewById(R.id.metapolisi);




        ImerominaText.setText(Imerominia);
        KouremaText.setText(Kourema);
        XtenismaText.setText(Xtenisma);
        BafiText.setText(Bafi);
        MetapolisiText.setText(Metapolis);
        return convertView;
    }
    }

