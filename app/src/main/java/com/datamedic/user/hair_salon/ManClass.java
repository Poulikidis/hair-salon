package com.datamedic.user.hair_salon;

public class ManClass {
    String imeromonia;
    String ipiresies;
    String metapolisi;
    private String Id;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }



    public String getImeromonia() {
        return imeromonia;
    }

    public void setImeromonia(String imeromonia) {
        this.imeromonia = imeromonia;
    }

    public String getIpiresies() {
        return ipiresies;
    }

    public void setIpiresies(String ipiresies) {
        this.ipiresies = ipiresies;
    }

    public String getMetapolisi() {
        return metapolisi;
    }

    public void setMetapolisi(String metapolisi) {
        this.metapolisi = metapolisi;
    }


    public ManClass(String imeromonia,String ipiresia,String metapolisi,String id){
       this.imeromonia = imeromonia;
       this.ipiresies = ipiresia;
       this.metapolisi = metapolisi;
       this.Id = id;
    }

}
