package com.datamedic.user.hair_salon;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.datamedic.user.hair_salon.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.IOException;


public class Man_Women extends MainActivity {

    Button man,women,select,manView,womanView, expenses,expensesView;
    Database database;
    RotateResizeImage rotateResizeImage;

    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.man_women);


        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        sharedPreferences = this.getSharedPreferences("com.datamedic.userx.ratingapp", Context.MODE_PRIVATE);
        String picturePath = sharedPreferences.getString("picturepath1","");
        ConstraintLayout constraintLayout = findViewById(R.id.constraint2);
        rotateResizeImage = new RotateResizeImage();


        if(picturePath == null || picturePath.trim().equals("")){



        }else{
            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            rotateResizeImage.getResizedBitmap(bitmap,150,150);
            BitmapDrawable background = null;
            try {
                background = new BitmapDrawable(rotateResizeImage.modifyOrientation(bitmap,picturePath));
            } catch (IOException e) {
                e.printStackTrace();
            }


            constraintLayout.setBackground(background);
        }


        database = new Database(this);
        
        man = findViewById(R.id.man);
        women = findViewById(R.id.women);
        select = findViewById(R.id.select);
        manView = findViewById(R.id.ManView);
        womanView = findViewById(R.id.WomanView);
        expenses = findViewById(R.id.expenses);
        expensesView = findViewById(R.id.expenseslist);


        expenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(),Expenses.class);
                startActivity(intent);
                finish();
            }
        });

        
        man.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(),Man.class);
                startActivity(intent);
                finish();
            }
        });
//        viewAll();
        
        women.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(),Women.class);
                startActivity(intent);
                finish();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(),Select.class);
                startActivity(intent);
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }

                finish();
            }
        });

        manView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(),ManList.class);
                startActivity(intent);
                finish();

            }
        });

        womanView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(),WomanList.class);
                startActivity(intent);
                finish();
            }
        });
        expensesView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(),ExpensesList.class);
                startActivity(intent);
                finish();
            }
        });

    }

//
//    public void viewAll(){
//        man.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Cursor res = database.getMan();
//                if (res.getCount()==0){
//                    show("Error","Nothing FOund");
//                    return;
//                }
//                StringBuffer buffer = new StringBuffer();
//                while (res.moveToNext()){
//                    buffer.append("ΗΜΕΡΟΜΗΝΙΑ:"+res.getString(1)+"\n");
//                    buffer.append("ΥΠΗΡΕΣΙΕΣ:"+res.getString(2)+"\n");
//                    buffer.append("ΜΕΤΑΠΩΛΗΣΗ:"+res.getString(3)+"\n");
//                }
//                show("data",buffer.toString());
//            }
//        });
//    }
//    public void show (String title, String mess){
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setCancelable(true);
//        builder.setMessage(mess);
//        builder.setTitle(title);
//        builder.show();
//
//
//    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

}
