package com.datamedic.user.hair_salon;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Select extends MainActivity {


    TextView displayFrom, displayTo, resultAll, resultMan, resultWomen, resultExpenses, diafora;
    Calendar cal;
    Calendar cal2;
    Button selectAll, ypiresiesMan, metapolisisMan, xtenisma, kourema, bafi, metapolisisWomen, sinoloMan, sinoloWoman;
    DatePickerDialog.OnDateSetListener mDateListener, mDateListener2;
    public long c, c2;
    double resultManWomenFirst;
    double resultManFirst;
    double resultWomanFirst;
    double resultExpensesFirst;
    double resultKatharaEsoda;
    Database database;
    RotateResizeImage rotateResizeImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewlayout);
        database = new Database(this);
        sharedPreferences = this.getSharedPreferences("com.datamedic.userx.ratingapp", Context.MODE_PRIVATE);
        String picturePath = sharedPreferences.getString("picturepath1", "");
        rotateResizeImage = new RotateResizeImage();

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        ImageView imageView = findViewById(R.id.imageView);


        if (picturePath == null || picturePath.trim().equals("")) {

        } else {
            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            rotateResizeImage.getResizedBitmap(bitmap, 150, 150);
            BitmapDrawable background = null;
            try {
                background = new BitmapDrawable(rotateResizeImage.modifyOrientation(bitmap, picturePath));

            } catch (IOException e) {
                e.printStackTrace();
            }
            imageView.setBackground(background);
        }


        database = new Database(this);


        displayFrom = findViewById(R.id.from);
        displayTo = findViewById(R.id.to);
        selectAll = findViewById(R.id.SelectAll);
        ypiresiesMan = findViewById(R.id.ypiresiesMan);
        metapolisisMan = findViewById(R.id.MetapolisiMan);
        xtenisma = findViewById(R.id.XtenismaWomen);
        kourema = findViewById(R.id.KouremaWomen);
        bafi = findViewById(R.id.bafiWomen);
        metapolisisWomen = findViewById(R.id.metapolisiWomen);
        resultAll = findViewById(R.id.resultAll);
        resultMan = findViewById(R.id.resultMan);
        resultWomen = findViewById(R.id.resultWoman);
        sinoloMan = findViewById(R.id.sinoloMan);
        sinoloWoman = findViewById(R.id.sinoloWoman);
        resultExpenses = findViewById(R.id.resultExpens);
        diafora = findViewById(R.id.diaforatextView);


        selectAll.setOnClickListener(new button());
        ypiresiesMan.setOnClickListener(new button());
        metapolisisMan.setOnClickListener(new button());
        kourema.setOnClickListener(new button());
        xtenisma.setOnClickListener(new button());
        bafi.setOnClickListener(new button());
        metapolisisWomen.setOnClickListener(new button());
        sinoloMan.setOnClickListener(new button());
        sinoloWoman.setOnClickListener(new button());


        cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 00);
        cal.set(Calendar.MINUTE, 00);
        cal2 = Calendar.getInstance();
        cal2.set(Calendar.HOUR_OF_DAY, 23);
        cal2.set(Calendar.MINUTE, 59);
        c = cal.getTimeInMillis();
        c2 = cal2.getTimeInMillis();


        resultManFirst = database.selectManAll(c, c2);

        resultWomanFirst = database.selectWomanAll(c, c2);

        resultExpensesFirst = database.selectExpensesAll(c, c2);

        resultManWomenFirst = resultManFirst + resultWomanFirst;

        resultKatharaEsoda = (resultManFirst + resultWomanFirst) - resultExpensesFirst;

        resultAll.setText(String.valueOf(resultManWomenFirst));
        resultMan.setText(String.valueOf(resultManFirst));
        resultWomen.setText(String.valueOf(resultWomanFirst));
        resultExpenses.setText(String.valueOf(resultExpensesFirst));
        diafora.setText(String.valueOf(resultKatharaEsoda));



        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());

        displayFrom.setText(date);
        displayTo.setText(date);


        displayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog dialog = new DatePickerDialog(Select.this, android.R.style.Theme_Holo_Dialog, mDateListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();


            }
        });
        mDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {


                cal.set(year, month, dayOfMonth, 00, 00);
                month += 1;
                String date = dayOfMonth + "/" + month + "/" + year;
                displayFrom.setText(date);
                c = cal.getTimeInMillis();

            }
        };


        displayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cal2 = Calendar.getInstance();
                int year = cal2.get(Calendar.YEAR);
                int month = cal2.get(Calendar.MONTH);
                int day = cal2.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(Select.this, android.R.style.Theme_Holo_Dialog, mDateListener2, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

            }
        });
        mDateListener2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                cal2.set(year, month, dayOfMonth, 23, 59);
                month += 1;
                String Date = dayOfMonth + "/" + month + "/" + year;
                displayTo.setText(Date);
                c2 = cal2.getTimeInMillis();


            }
        };


    }

    public class button implements View.OnClickListener {


        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.SelectAll:
                    double man = database.selectManAll(c, c2);
                    double woman = database.selectWomanAll(c, c2);
                    double expense = database.selectExpensesAll(c, c2);
                    double allWithExpens = (man + woman) - expense;
                    double resultAllesoda = man + woman;
                    double expenses = database.selectExpensesAll(c, c2);
                    String allExpenses = String.valueOf(expenses);
                    String result = String.valueOf(resultAllesoda);
                    String resultAllWithExpens = String.valueOf(allWithExpens);
                    resultExpenses.setText(allExpenses);
                    resultAll.setText(result);
                    diafora.setText(resultAllWithExpens);
                    break;
                case R.id.ypiresiesMan:
                    double ypiresies = database.selectMan("ΥΠΗΡΕΣΙΕΣ", c, c2);
                    String result2 = String.valueOf(ypiresies);
                    resultMan.setText(result2);
                    break;
                case R.id.MetapolisiMan:
                    double metapolisis = database.selectMan("ΜΕΤΑΠΩΛΗΣΗ_ΑΝ", c, c2);
                    String result3 = String.valueOf(metapolisis);
                    resultMan.setText(result3);
                    break;
                case R.id.KouremaWomen:
                    double kourema = database.selectWomen("ΚΟΥΡΕΜΑ", c, c2);
                    String result4 = String.valueOf(kourema);
                    resultWomen.setText(result4);
                    break;
                case R.id.XtenismaWomen:
                    double xtenisma = database.selectWomen("ΧΤΕΝΙΣΜΑ", c, c2);
                    String result5 = String.valueOf(xtenisma);
                    resultWomen.setText(result5);
                    break;
                case R.id.bafiWomen:
                    double bafi = database.selectWomen("ΒΑΦΗ", c, c2);
                    String result6 = String.valueOf(bafi);
                    resultWomen.setText(result6);
                    break;
                case R.id.metapolisiWomen:
                    double metapolisiWoman = database.selectWomen("ΜΕΤΑΠΩΛΗΣΗΣ_ΓΥΝ", c, c2);
                    String result7 = String.valueOf(metapolisiWoman);
                    resultWomen.setText(result7);
                    break;
                case R.id.sinoloMan:
                    double sinoloMan = database.selectManAll(c, c2);
                    String result8 = String.valueOf(sinoloMan);
                    resultMan.setText(result8);
                    break;
                case R.id.sinoloWoman:
                    double sinoloWoman = database.selectWomanAll(c, c2);
                    String result9 = String.valueOf(sinoloWoman);
                    resultWomen.setText(result9);
                    break;



            }
        }

    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), Man_Women.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

}
