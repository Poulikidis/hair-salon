package com.datamedic.user.hair_salon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ExpensesClassList extends ArrayAdapter<ExpensesClass> {

    private Context mContext;
    private int mresource;

    Database database;


    public ExpensesClassList(Context context, int resource, ArrayList object) {
        super(context, resource,object);
        mContext = context;
        mresource = resource;
        database = new Database(context);
    }


    @Override
    public View getView(int position,View convertView, ViewGroup parent) {
        final String Imerominia = getItem(position).getImerominia();
        final String Tipos = getItem(position).getTipos();
        final String Expenses = getItem(position).getExpenses();
        final String ID = getItem(position).getId();

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mresource, parent, false);

        TextView ImerominaText = convertView.findViewById(R.id.calendarExpenses);
        TextView TiposText = convertView.findViewById(R.id.what);
        TextView ExpensesText = convertView.findViewById(R.id.expenses);




        ImerominaText.setText(Imerominia);
        TiposText.setText(Tipos);
        ExpensesText.setText(Expenses);
        return convertView;
    }
}
