package com.datamedic.user.hair_salon;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.datamedic.user.hair_salon.R;
import com.google.android.gms.ads.AdRequest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Man extends MainActivity {

    EditText Ypiresies, Metapolisi;
    Button save;
    Database database;
    RotateResizeImage rotateResizeImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.man_layout);
        sharedPreferences = this.getSharedPreferences("com.datamedic.userx.ratingapp", Context.MODE_PRIVATE);
        String picturePath = sharedPreferences.getString("picturepath1", "");

        rotateResizeImage = new RotateResizeImage();

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        ImageView imageView = findViewById(R.id.imageView);


        if (picturePath == null || picturePath.trim().equals("")) {

        } else {
            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            rotateResizeImage.getResizedBitmap(bitmap,150,150);
            BitmapDrawable background = null;
            try {
                background = new BitmapDrawable(rotateResizeImage.modifyOrientation(bitmap, picturePath));

            } catch (IOException e) {
                e.printStackTrace();
            }
            imageView.setBackground(background);
        }


//        BlurImage.with(getApplicationContext()).load(R.drawable.marinos).intensity(10).Async(true).into(imageView);
        database = new Database(this);


        Ypiresies = findViewById(R.id.editText);
        Metapolisi = findViewById(R.id.editText2);

        save = findViewById(R.id.button4);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double a;
                double b;
                long date = Calendar.getInstance().getTimeInMillis();


                String imerominia = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());


                Log.d("imerominia ", imerominia);

                if (!Ypiresies.getText().toString().equals(""))
                    a = Double.parseDouble(Ypiresies.getText().toString());
                else
                    a = 0;

                if (!Metapolisi.getText().toString().equals(""))
                    b = Double.parseDouble(Metapolisi.getText().toString());
                else
                    b = 0;


                boolean insertet = database.data(imerominia, a, b, date);
                if (insertet) {
                    Intent i = new Intent(Man.this, Man_Women.class);
                    startActivity(i);
                    finish();
                    Toast.makeText(getApplicationContext(), R.string.imputssaved, Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getApplicationContext(), R.string.somethingwentwrong, Toast.LENGTH_SHORT).show();
            }
        });


    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), Man_Women.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }
}

