package com.datamedic.user.hair_salon;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.datamedic.user.hair_salon.R;
import com.google.android.gms.ads.AdRequest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Women extends MainActivity {

    EditText Kourema, Xtenisma, Bafi, Metapolisi;
    Button Save;
    Database database;
    RotateResizeImage rotateResizeImage;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.women_layout);
        database = new Database(this);
        sharedPreferences = this.getSharedPreferences("com.datamedic.userx.ratingapp", Context.MODE_PRIVATE);
        String picturePath = sharedPreferences.getString("picturepath1","");
        rotateResizeImage = new RotateResizeImage();

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        ImageView imageView = findViewById(R.id.imageView);


        if(picturePath == null || picturePath.trim().equals("")){

        }else {
            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            rotateResizeImage.getResizedBitmap(bitmap,150,150);
            BitmapDrawable background = null;
            try {
                background = new BitmapDrawable(rotateResizeImage.modifyOrientation(bitmap, picturePath));

            } catch (IOException e) {
                e.printStackTrace();
            }
            imageView.setBackground(background);
        }
//        BlurImage.with(getApplicationContext()).load(R.drawable.marinos).intensity(20).Async(true).into(imageView);

        Kourema = findViewById(R.id.Kourema);
        Xtenisma = findViewById(R.id.Xtenisma);
        Bafi = findViewById(R.id.Bafes);
        Metapolisi = findViewById(R.id.Metapolisi);

        Save = findViewById(R.id.button3);

        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double a;
                double b;
                double c;
                double d;
                long date = Calendar.getInstance().getTimeInMillis();
                String imerominia = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());

                if (!Kourema.getText().toString().equals(""))
                    a =  Double.parseDouble(Kourema.getText().toString());
                else
                    a = 0;

                if (!Xtenisma.getText().toString().equals(""))
                    b = Double.parseDouble(Xtenisma.getText().toString());
                else
                    b = 0;

                if (!Bafi.getText().toString().equals(""))
                    c = Double.parseDouble(Bafi.getText().toString());
                else
                    c = 0;

                if (!Metapolisi.getText().toString().equals(""))
                    d = Double.parseDouble(Metapolisi.getText().toString());
                else
                    d = 0;


                boolean insertet = database.data2(imerominia,a, b, c, d,date);
                if (insertet) {
                    Intent i = new Intent(Women.this, Man_Women.class);
                    startActivity(i);
                    finish();
                    Toast.makeText(getApplicationContext(), R.string.imputssaved, Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getApplicationContext(), R.string.somethingwentwrong, Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), Man_Women.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }
}
