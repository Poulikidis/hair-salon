package com.datamedic.user.hair_salon;

public class ExpensesClass {

    String imerominia, tipos, expenses, id;


    public String getImerominia() {
        return imerominia;
    }

    public void setImerominia(String imerominia) {
        this.imerominia = imerominia;
    }

    public String getTipos() {
        return tipos;
    }

    public void setTipos(String tupos) {
        this.tipos = tupos;
    }

    public String getExpenses() {
        return expenses;
    }

    public void setExpenses(String expenses) {
        this.expenses = expenses;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public ExpensesClass(String imeromina, String tipos, String expenses, String id) {
        this.imerominia = imeromina;
        this.tipos = tipos;
        this.expenses = expenses;
        this.id = id;
    }
}
